var url="https://github.com/pubnub/python/search?la=us−en&q=python";
var output = {};

// Split Path and Parms
var pathandParms = url.split("?");

// Get Path
var rawPath = pathandParms[0];

// Get Parms
var rawParms = pathandParms[1];

// extracting the query part
var Query = "?"+rawParms;



// Get Host out of Path
var hostandPath = rawPath.split(":");
var host = hostandPath[0];



// Get Filtered Path
var rawfilteredPath = hostandPath[1].replace("//",'');

// Spliting the raw path to get the hostname and path
var splitedfilteredPath = rawfilteredPath.split('/');

// Getting host name and path

var filteredHostname = splitedfilteredPath[0];


// Deleting the host name from path
splitedfilteredPath.shift();
// Joining the path again
var Path = splitedfilteredPath.join('/');



// Spliting the path to get the file
var file = splitedfilteredPath[splitedfilteredPath.length-1];


// Spliting the path to get segments 
var segments = Path.split('/');

// Adding query and path to get relative
 var relative = Query + Path;


// extracing params from raw params
var params = {};

// spliting the raw params
var splited_raw_params = rawParms.split('&');
// spliting for la 
var la_params = splited_raw_params[0].split('=');
// spliting for q 
var q_params = splited_raw_params[1].split('=');

// assigning to the params object
params[la_params[0]] = la_params[1];
params[q_params[0]] = q_params[1];


// Set to output
output["Source"] = url;
output["Protocol"] = host;
output["Host"] = filteredHostname;
output["Query"] = Query;
output["Params"] = params;
output["File"] = file;
output["Path"] = Path;
output["Relative"] = relative;
output["Segments"] = segments;



console.log(output);