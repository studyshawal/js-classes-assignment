
//display properties function
function PropDisplay(ObjectName) {
    var x;
    var text="";
    //  getting all the properties from the obj and saving it in the text variable
    for (x in ObjectName) {
        text += ObjectName[x] + " ";
    }
    console.log(text);
}


//object definition
var student= {
    name:"Shawal Nazir",
    sclass:"10", 
    rollno:12,
    subjects:"english"

};


PropDisplay(student);
module.exports.PropDisplay = PropDisplay;
