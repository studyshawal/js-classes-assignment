var library = [
    {
        author : 'Bill Gates',
        title :'The Road Ahead',
        readingStatus: false
    },
       
    {
        author : 'Steve Jobs',
        title :'Walter Isaacson',
        readingStatus: true
    },
    
    {
        author : 'Suzanne Collins',
        title :'Mockingjay : The Final Book of The Hunger Games',
        readingStatus: false
    }];


    

//accessing each element of the array objects for comparisons
   for( var x=0;x<library.length;x++){
        if (library[x].readingStatus==true) {
        console.log(library[x].title+' by '+library[x].author+' has been read before.');
        }

        else{
        console.log(library[x].title+' by '+library[x].author+' has not been read before.');
        }
    }